var express = require('express');
var ect = require('ect');
var path = require('path');

var util = require('util');
var fs = require('fs');


var app = express();

// *** Configuration ***
var ectRenderer = ect({ watch:true, ext: '.html', root:__dirname + 'dist'});
app.engine('.html', ectRenderer.render);

app.set('port', process.env.PORT || 8080);

app.use(express.static(path.join(__dirname, 'dist')));
console.log('ngCore app started');

var dir = __dirname;
app.set('views', __dirname);

app.listen(app.get('port'), function() {
    console.log("Express server listening on port " + app.get('port'));
});
