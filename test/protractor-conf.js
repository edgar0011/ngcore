exports.config = {
  //IMPORTANT, version of the lib can change in time
  seleniumServerJar: '../node_modules/protractor/selenium/selenium-server-standalone-2.45.0.jar',
  chromeDriver: '../node_modules/protractor/selenium/chromedriver',

  // location of E2E test specs
  specs: [
    '../test/e2e/*.js'
  ],
  
  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://localhost:9000/',

  // testing framework, jasmine is the default, jasmine2 works with async setup better, jasmine reporting error on missing angular...
  framework: 'jasmine2'
};
