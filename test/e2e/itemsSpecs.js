
var ItemsPage = function() {

    this.footer = element.all(by.css('div.footer-content'));

    this.menuList = element.all(by.repeater('menuItem in appCtrl.menuItems'));

    this.itemList = element.all(by.repeater('(noteId, note) in itemsCtrl.notes'));

};

describe('items', function() {
    browser.get('/#items');

    it("should load", function() {
        expect(browser.getTitle()).toBe('ngCore');
    });

    var itemsPage = new ItemsPage();

    describe('open page', function() {
        it('should have a displayed footer', function() {
            expect(itemsPage.footer.isDisplayed()).toBeTruthy();
        });

        it('should have an exactly two items in menu', function() {
            expect(itemsPage.menuList.count()).toEqual(2);
        });


        it('should have an exactly two items in items/notes', function() {
            //expect(itemsPage.itemList.count()).toEqual(2);
            expect(itemsPage.itemList.count()).toBeGreaterThan(0);
        });

    });

});