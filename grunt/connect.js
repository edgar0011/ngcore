/**
 * Created by edgar on 04/06/15.
 */

var modRewrite = require('connect-modrewrite');
var serveStatic = require('serve-static');

var mountFolder = function (connect, dir) {
    console.log("dir: " + dir);
    console.log("require('path').resolve(dir): " + require('path').resolve(dir));
    return serveStatic(require('path').resolve(dir));
};
module.exports.tasks = {
    connect : {
        server: {
            options: {
                port: 9000,
                keepalive: false,
                livereload: 35729,
                open:"http://localhost:9000",
                base: '<%= config.dist %>',
                directory:".",
                hostname: 'localhost',
                middleware: function (connect) {
                    return [
                        modRewrite (['!\\/delegate|\\.html|\\.ts|\\.js|\\.svg|\\.less|\\.gif|\\.css|\\.png|\\.jpg|\\.jpeg|\\.woff$ /index.html [L]']),
                        mountFolder(connect, 'dist')
                    ];
                }
            }
        }
    }
};