angular.module("ngCore.controllers", [])

.controller("AppController", function (
        $scope, $rootScope, $translate, $timeout, $location, $locale,
        tmhDynamicLocale, edMessageBus, edAlertMessageConstants, edCoreConstants

    ) {


    $rootScope.model = {loaded: true};

    $rootScope.modelTotal = {loaded :true};

    //translation
    this.langKey = $translate.proposedLanguage();
    if (!this.langKey) {
        this.langKey = $translate.use();
    }

    tmhDynamicLocale.set(this.langKey);

    this.changeLanguage = function (langKey) {
        this.langKey = langKey;
        $translate.use(langKey);
        tmhDynamicLocale.set(langKey);
    };

    this.isLogged = false;

    this.menuItems = [

        {
            state: "items",
            locale: "ngCore.ITEMS_LABEL"

        },
        {
            state: "help",
            locale: "ngCore.HELP_LABEL"

        }

    ];


    setTimeout(function () {
        edMessageBus.emit(
            edAlertMessageConstants.MESSAGE_EVT, {
                message: "Welcome",
                errorLevel: 2,
                context: edCoreConstants.errorMessageContexts.USER
            }
        );
    }, 100);

});
