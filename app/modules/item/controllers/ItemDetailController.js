/**
 * Created by edgar on 28/04/15.
 */
angular.module( "ngCore.modules.item" )

    .controller( "ItemDetailController", ItemDetailController );

function ItemDetailController( $stateParams, $log, Note ) {

    this.note = null;

    Note.get( { noteId:$stateParams.noteId } ).$promise.then( function( note ) {

        this.note = note;

    }.bind( this ), function( reason ) {

        $log.error( "Note get failed, noteId " + reason + ", " + $stateParams.noteId );

    } );

}
