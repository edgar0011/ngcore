/**
 * Created by edgar on 28/04/15.
 */
angular.module( "ngCore.modules.item" )

.controller( "ItemsController", ItemsController );

function ItemsController( $document, $filter, $log, edMessageBus, edCoreConstants, edAlertMessageConstants, Note ) {

    this.title = "Items Controller";

    this.newNote = {};

    //Note.auth();

    this.getNotes = function() {

        var res = Note.getNotes();
        res.then(
            function( response ) {

                this.notes = response;
                $log.debug( response );

            }.bind( this ),
            function( reason ) {

                $log.error( reason );

            }

        );

    };


    this.addNote = function( note ) {

        note.created = ( new Date() ).getTime();

        var newNote = new Note( note );
        //newNote.$save().then(
        Note.save( newNote ).$promise.then(
            function( response ) {

                $log.debug( response );
                this.getNotes();

                edMessageBus.emit(
                    edAlertMessageConstants.MESSAGE_EVT, {
                        message: "ngCore.items.message.NOTE_ADDED",
                        errorLevel: 2,
                        context: edCoreConstants.errorMessageContexts.USER
                    }
                );

            }.bind( this ),
            function( reason ) {

                $log.error( reason );

            }
        );

        this.newNote = {};

    };

    this.removeNote = function( noteId ) {

      Note.remove( { noteId:noteId } ).$promise.then(
            //item.$remove().$promise.then(
            function( response ) {

                $log.debug( response );
                this.getNotes();

                edMessageBus.emit(
                    edAlertMessageConstants.MESSAGE_EVT, {
                        message: "ngCore.items.message.NOTE_REMOVED",
                        errorLevel: 2,
                        context: edCoreConstants.errorMessageContexts.USER
                    }
                );

            }.bind( this ),
            function( reason ) {

                $log.error( reason );

            }

        );

    };

    this.updateNote = function( noteId, data ) {

        data.updated = ( new Date() ).getTime();
        data.editing = null;

        Note.update( { noteId:noteId }, data ).$promise.then(
            //item.$remove().$promise.then(
            function( response ) {

                $log.debug( response );
                this.getNotes();

                edMessageBus.emit(
                    edAlertMessageConstants.MESSAGE_EVT, {
                        message: "ngCore.items.message.NOTE_UPDATED",
                        errorLevel: 2,
                        context: edCoreConstants.errorMessageContexts.USER
                    }
                );

            }.bind( this ),
            function( reason ) {

                $log.error( reason );

            }
        );

    };

    this.askRemoveNote = function( noteId, note ) {

        if ( confirm( $filter( 'translate' )( 'core.DELETE_CONFIRMATION' ) + " "  + note.title + " ?" ) ) {

            this.removeNote( noteId );

        }

    };

    this.showAddNoteUI = function() {

        var someElement = angular.element( document.getElementById( 'addNote' ) );
        $document.scrollToElement( someElement, 70, 500 );

    };

    /**
     *
     * Dynamic module, test
     *
     */
    this.createModule = function() {

        angular.module( "testModule", [ 'ngResource' ] )
            .controller( "TestModuleController", function() {


            } )
            .directive( "testModuleDirective", function() {

                return {

                    link:function( scope ) {

                    }

                };

            } );

    };

    this.checkModule = function() {

        var testModule = angular.module( "testModule" );

        alert( testModule );

    };

    /**
     * load data
     */
    this.getNotes();

}

