
var app = angular.module( 'ngCore', [
    "ngResource",
    "ngSanitize",
    "ngCookies",
    "ui.bootstrap",
    "ngAnimate",

    "ui.router",
    'ui.select',
    'duScroll',

    "ngCore.i18n",
    'ngCore.controllers',
    //'ngCore.templates',

    'pascalprecht.translate',
    'tmh.dynamicLocale',

    'ed.ui',

    //APP modules
    "ngCore.modules.item",
    "ngCore.modules.dashboard",
    "ngCore.modules.help",
    "ngCore.components"
] );

app.value( "NON_INTERCEPTING_URLS", [ ".htm" ] );

/*@ngInject*/
app.config(
    function(
        $stateProvider,
        $urlRouterProvider,
        $httpProvider,
        $logProvider,
        $translateProvider, tmhDynamicLocaleProvider ) {

        $logProvider.debugEnabled( false );

        $urlRouterProvider
            .when( '/', '/items' );
           /* .otherwise('/');*/

        $urlRouterProvider.otherwise( function( $injector, $location ) {
            
                $location.path( "/" );

            }
        );

        $stateProvider

            .state( 'items', {
                url: '/items',
                views:{
                    main: {
                        templateUrl:  'modules/item/partials/items.html',
                        controller:"ItemsController",
                        controllerAs:"itemsCtrl"
                    },
                    footer:{
                        template:"2015 ngCore - ITEMS"
                    }
                }
            })

            .state( 'detail', {
                url: '/item/:noteId',
                views:{
                    main: {
                        templateUrl:  'modules/item/partials/itemdetail.html',
                        controller:"ItemDetailController",
                        controllerAs:"itemDetailCtrl"
                    },
                    footer:{
                        template:"2015 ngCore - ITEMS"
                    }
                }
            })

            .state('dashboard', {
                url:'/dashboard',
                views:{
                    main:{
                        templateUrl:"modules/dashboard/partials/dashboard.html",
                        controllerAs:'dashCtrl',
                        controller:"DashboardController"

                    },
                    footer:{
                        template:"2015 ngCore - HELP"
                    }
                }
            })


            .state('help', {
                url:'/help',
                views:{
                    main:{
                        templateUrl:"modules/help/partials/help.html",
                        controllerAs:'helpCtrl',
                        controller:"HelpController"

                    },
                    footer:{
                        template:"2015 ngCore - HELP"
                    }
                }
            });


        /**
         * Localization...
         */
        $translateProvider.useLocalStorage();
        $translateProvider.preferredLanguage('cs');
        $translateProvider.fallbackLanguage('en');

        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });

        $translateProvider.storagePrefix('ngCore');

        $translateProvider.useSanitizeValueStrategy('escaped');

        tmhDynamicLocaleProvider.localeLocationPattern('assets/angular-i18n/angular-locale_{{locale}}.js');

        $httpProvider.interceptors.push("ngCoreInterceptor");
    });

/*@ngInject*/
app.factory("ngCoreInterceptor", function($q, $rootScope, NON_INTERCEPTING_URLS) {
    return {
        'request': function(config) {
            var showLoading = true;
            for (var i in NON_INTERCEPTING_URLS) {
                if (config.url.indexOf(NON_INTERCEPTING_URLS[i]) > -1 ) {
                    showLoading = false;
                    break;
                }
            }
            if (showLoading) {
                $rootScope.$broadcast("resourceLoading");
            }

            return config || $q.when(config);
        },

        // optional method
        'requestError': function(rejection) {
            $rootScope.$broadcast("resourceLoaded");
            return $q.reject(rejection);
        },

        // optional method
        'response': function(response) {
            //alert("service response");
            var showLoading = true;
            for (var i in NON_INTERCEPTING_URLS) {
                if (response.config.url.indexOf(NON_INTERCEPTING_URLS[i]) > -1 ) {
                    showLoading = false;
                    break;
                }
            }
            if (showLoading) {
                $rootScope.$broadcast("resourceLoaded");
            }
            return response || $q.when(response);
        },

        // optional method
        'responseError': function(rejection) {
            //alert("service responseError");
            $rootScope.$broadcast("resourceLoaded");
            return $q.reject(rejection);
        }
    };
});

/*@ngInject*/
app.run(function ($rootScope, $state, $stateParams, $timeout, $translate, $log) {

    $rootScope.model = {loaded:false};

    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            //alert("stateChangeStart " + JSON.stringify(toState));
            $rootScope.$broadcast("resourceLoading");
        });

    $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams){
            //alert("$stateChangeSuccess " + JSON.stringify(toState));
            $rootScope.$broadcast("resourceLoaded");
        });

    $rootScope.$on('$stateChangeError',
        function(event, toState, toParams, fromState, fromParams, error){
            //alert("$stateChangeError " + JSON.stringify(error));

            $rootScope.$broadcast("resourceLoaded");
        });

    $rootScope.$on('$stateNotFound',
        function(event, unfoundState, fromState, fromParams){
            //alert("$stateNotFound " + JSON.stringify(unfoundState));
            $rootScope.$broadcast("resourceLoaded");
        });


    $rootScope.$on("resourceLoading", function (event, data){
        $log.debug("LOADING");
        $rootScope.model.loaded = false;
    });


    //respond to resource loading event, i.e. show loading spinner
    $rootScope.$on("resourceLoaded", function (event, data){
        $timeout(function() {
            $rootScope.model.loaded = true;
            $log.debug("LOADED");
        }, 200);
    });

    //Reload translations on change
    $rootScope.$on('$translatePartialLoaderStructureChanged', function() {
        $translate.refresh();
    });

    $translate.refresh();

});


/*@ngInject*/
app.factory("lodash", function($window) {

    var lodash = $window._;

    delete $window._;

    return lodash;

});

/*@ngInject*/
app.run(function(lodash) {

    //alert(lodash);

});

/*@ngInject*/
angular.module('ngCore.i18n', ['pascalprecht.translate']).run(function($translatePartialLoader) {
    $translatePartialLoader.addPart('assets/locale');
});

app.animation('.animated-show-item', function() {

    return {
        //addClass: function(element, className, done) {
        beforeAddClass: function(element, className, done) {
            //addClass : function(element, className, done) {
            //alert("animated-show-item beforeAddClass " + className);
            if (className == 'ng-hide') {
                TweenMax.to(element, 0.4, {
                    opacity: 0,
                    scaleX: 1.5,
                    scaleY: 1.5,
                    ease: Power2.easeOut,
                    onComplete: function() {
                        //alert("ANIM on beforeAddClass DONE");
                        done();
                    }
                });
            } else {
                done();
            }
        },
        removeClass: function(element, className, done) {
            //alert("animated-show-item removeClass " + className);

            if (className == 'ng-hide') {

                element.css('opacity', 0);
                TweenMax.set(element, {
                    scale: 1.5
                });

                TweenMax.to(element, 0.4, {
                    opacity: 1,
                    scaleX: 1,
                    scaleY: 1,
                    onComplete: done
                });
            } else {
                done();
            }
        }
    };
});

app.animation('.animated-enter-item', function() {

    return {

        leave: function(element, done) {
            //alert("animated-enter-item element " + element);
            TweenMax.to(element, 0.4, {
                opacity: 0,
                scale: 1.1,
                transformOrigin: "50% 50%",
                y:"20px",
                ease: Power2.easeOut,
                onComplete: function() {
                    //alert("ANIM on beforeAddClass DONE");
                    done();
                }
            });

        },
        enter: function(element, done) {
            //alert("animated-enter-item element " + element);
            TweenMax.set(element,  {
                    transformOrigin: "50% 50%",
                    scale:1.1,
                    y:"20px"}
            );
            element.css({'opacity': 0.5, 'display': 'block'});
            TweenMax.to(element, 0.6, {
                opacity: 1,
                scale:1,
                //transformOrigin: "50% 50%",
                y:"0px",
                onComplete: done
            });


        },
        setup : function(element) {
            console.log("animation .animated-enter-item: setup");
        },
        start : function(element, done, memo) {
            console.log("animation .animated-enter-item: start");
        },
        cancel : function(element, done) {
            console.log("animation .animated-enter-item: cancel");
        },
        beforeAddClass: function(element, className, done) {
            console.log("animation .animated-enter-item: beforeAddClass: " + className);
        }
    };

});

//wait for app templates code, added at the end...
//could be solved by anitehr module, e.g. ngCore.templates, declared in ngTemplates bootstrap,
// and as dependency to ngCore, but in develop mode, such modul would have to be created
setTimeout(function(){
    angular.bootstrap(document, ['ngCore'], {
        strictDi: false
    });
}, 0);
