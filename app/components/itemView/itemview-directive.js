/**
 * Created by edgar on 28/04/15.
 */

angular.module("ngCore.components.ui.item", [])


.directive("itemView", function(){

        return {

            restrict:"E",
            scope:{
                item:"=item"
            },

            templateUrl:"components/itemView/itemview-view.html"

        };


    });