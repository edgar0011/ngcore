/**
 * Created by edgar on 29/04/15.
 */

angular.module( "ngCore.components.note", [] )

    .constant( 'API_BASE_URL', 'https://1389574865048.firebaseio.com/notes.json' )

    .constant( 'API_URL', 'https://1389574865048.firebaseio.com/notes/:noteId.json' )

    .factory( "Note", function( $resource, API_URL, API_BASE_URL, $http ) {

        var res =  $resource( API_URL, null, {

            'update': {

                method:'PUT'

            }

        } );


        function auth () {

            var firebaseConfig = {
                apiKey: "1389574865048",
                authDomain: "1389574865048.firebaseapp.com",
                databaseURL: "https://1389574865048.firebaseio.com"
            };

            var FbApp = firebase.initializeApp(firebaseConfig);

            var ref = FbApp.database();
            ref.authWithOAuthPopup("google", function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                }
            });
        }

        function getNotes() {

            //in case of firebase API, get is used instead of query
            return res.get().$promise;

        }

        function getNotes2() {

            return $http.get( API_BASE_URL + "X" ).
                success( function( data, status, headers, config ) {

                    // this callback will be called asynchronously
                    // when the response is available
                    return data;

                } ).
                error( function( data, status, headers, config ) {

                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    return "Error";

                } )
                .then( function( response ) {

                    return response.data;

                }, function( reason ) {

                } );

        }

        return angular.extend(res, {
            getNotes:getNotes,
            getNotes2:getNotes2,
            auth:auth
        });

    }
);
