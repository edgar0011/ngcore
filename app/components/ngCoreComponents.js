/**
 * Created by edgar on 28/04/15.
 */
angular.module( 'ngCore.components', [
    'ngCore.components.ui', 'ngCore.components.utils',
    'ngCore.components.note' ] );

angular.module( 'ngCore.components.ui', [ "ngCore.components.ui.item" ] );

angular.module( 'ngCore.components.utils', [] );

